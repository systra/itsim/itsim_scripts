.PHONY: default clean venv node_modules bump_version build twine test_upload pypi_upload

default:
	@echo "make TARGET"
	@echo ""
	@echo "TARGETS:"
	@echo "  clean: delete all generated files"
	@echo "  bump_version: use the 'what' variable to define what to bump: major, minor or patch"
	@echo "  build: create source and wheel packages"
	@echo "  test_upload: build and upload packages to testpypi (always do this first)"
	@echo "  pypi_upload: build and upload packages to pypi"

clean:
	@rm -rf build dist src/*.egg-info src/itsim_scripts/node_modules 2>/dev/null
	@find src -type d -name __pycache__ -prune -exec rm -rf '{}' \;

venv:
	@if [ -z "$$VIRTUAL_ENV" ]; then \
	    echo "You should activate the virtualenv: pipenv shell" >&2; \
	    exit 1; \
	fi

bump_version: venv
	@if ! echo "$(what)" | grep -q '^major\|minor\|patch$$'; then \
	    echo "You should specify 'what' variable with one of major, minor or patch" >&2; \
	    exit 1; \
	fi; \
	VER_MODULE="$$(sed -rn '/^version =/{s/.* attr: (.*)/\1/p}' setup.cfg | rev | cut -d. -f2- | rev)"; \
	VER_VAR="$$(sed -rn '/^version =/{s/.* attr: (.*)/\1/p}' setup.cfg | rev | cut -d. -f1 | rev)"; \
	NEW_VER="$$(python -c 'from src.'$${VER_MODULE}' import '$${VER_VAR}'; from semver import parse_version_info; print(parse_version_info(__version__).bump_$(what)())')"; \
	OLD_VER="$$(sed -rn "/^$${VER_VAR} =/s/.*'(.*)'/\1/p" src/$${VER_MODULE}/__init__.py)"; \
	echo "$${OLD_VER} → $${NEW_VER}"; \
	sed -ri "/^$${VER_VAR} =/s/'.*'/'$${NEW_VER}'/" src/$${VER_MODULE}/__init__.py

node_modules:
	@yarn --no-lockfile --modules-folder src/itsim_scripts/node_modules --non-interactive --silent --no-progress install

build: clean venv node_modules
	python setup.py sdist bdist_wheel

twine: venv
	@pip freeze | grep -q ^twine= >/dev/null || pip install twine

test_upload: build twine
	python -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*

pypi_upload: build twine
	python -m twine upload dist/*
