Itsim scripts library
=====================

How to use
----------

```
pip install itsim_scripts
```

Then in python
```
from itsim_scripts import utils
```

How to develop on it
--------------------

```
pip install -e .
```


Usage
-----

**add_layers:**

an example can be found for Jakarta, where an empty project had to be setup, and its population and jobs data added
later.
Steps :
- add the relevant layers with the `launcher` script in `backend/bootstrap_db`
- prepare the gis parameter configuration:
  - check the format of a gis parameter of a healthy project and copy it in a json file
    - edit it using the data that is in the json file you used in the first step (you used it to feed the `launcher` script)
- add the gis configuration, for exemple, when in this repository, launch:
```shell
./src/add_layers -u $URL --auth $AUTH_TOKEN -p $PROJECT_ID -d $PATH_TO_UPDATED_PROJECT_GIS_PARAMETER
```
where:
- `URL` is the url to the backend service where you want to do the update
- `AUTH_TOKEN` is an access token, such as you can generate using the swagger interface
- `PROJECT_ID` is the id of the project you want to update
- `PATH_TO_UPDATED_PROJECT_GIS_PARAMETER` is the path to a json file containing the updated GIS parameter for a project.
An example of such a file can be found in the itsim-projects-data repo, in the Jakarta directory.
