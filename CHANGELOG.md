# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

# Next
## Added
- script that helps adding gis layers configuration to an existing project
## Changed
- Update itsim authorization code for `check_layer_data` script

# 0.5.0
## Changed
- IT-197 Authorize decimals in legend parameters
- `gtfstk` library migrated to `gtfs_kit` library.

# 0.4.0
## Changed
- `create_json_project` with a new `--dest-dir` parameter.

# 0.3.1
## Fixed
- fix path error for `shp2geojson` inside `simplify_shapefile`

# 0.3.0
## Fixed
- another node path fix

# 0.2.0
## Fixed
- `simplify_shapefile` node script was not working.

# 0.1.0
Initialize library
